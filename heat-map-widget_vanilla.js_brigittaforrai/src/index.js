(function init () {

  const apiUrl = 'https://1x64nyq0q0.execute-api.us-east-1.amazonaws.com/dev/search';

  function createCORSRequest(method, url) {
    let xhr = new XMLHttpRequest();
    if ("withCredentials" in xhr) {
      // Check if the XMLHttpRequest object has a "withCredentials" property.
      // "withCredentials" only exists on XMLHTTPRequest2 objects.
      xhr.open('GET', apiUrl, true);
    } else if (typeof XDomainRequest != "undefined") {
      // Otherwise, check if XDomainRequest.
      // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
      xhr = new XDomainRequest();
      xhr.open(method, apiUrl);
    } else {
      // Otherwise, CORS is not supported by the browser.
      xhr = null;
    }
    return xhr;
  };

  function passDataToComponent (data) {
    let wrapper = document.querySelector('.wrapper');
    let component = document.createElement('heat-map');
    component.setAttribute('data', data); // add data attribute to the widget!
    wrapper.appendChild(component);
  };

  function httpGet (apiUrl, callback) {
    let xmlHttp = createCORSRequest('GET', apiUrl);
    if (!xmlHttp) {
      throw new Error('CORS not supported');
    }
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", apiUrl, true); // true for asynchronous
    xmlHttp.send(null);
  };

  httpGet(apiUrl, passDataToComponent);
})();
