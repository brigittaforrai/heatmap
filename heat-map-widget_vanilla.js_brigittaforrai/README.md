## Heat Map Widget by Brigitta Forrai


**Test widget with Vanilla javascript example**

1. Open terminal
2. [Download](https://www.python.org/downloads/) and install python if you do not have it on your computer. Or install via npm: *npm install -g python*
3. Go to the root folder of the project and start a python simpleHTTPServer:
4. Type *python -m SimpleHTTPServer 8080*
5. You can change the port (8080) if it is in use.
6. Open your browser and navigate to localhost:8080


##How to use Widget in any Vanilla javascript webpage##

**Add the following scripts and files in your website's index.html header:**

1. [d3.vr3.js](https://d3js.org/d3.v3.js) - D3 V3 svg library
2. [canvas-toBlob.js](https://cdn.rawgit.com/eligrey/canvas-toBlob.js/f1a01896135ab378aa5c0118eadd81da55e698d8/canvas-toBlob.js) - for exporting images
3. [FileSaver.js](https://cdn.rawgit.com/eligrey/FileSaver.js/e9d941381475b5df8b7d7691013401e171014e89/FileSaver.min.js) - for exporting images
4. [webcomponents.js](https://cdnjs.cloudflare.com/ajax/libs/webcomponentsjs/1.0.13/webcomponents-lite.js) - polyfill for web components
5. Link **heat-map-widget.html** (check in example code) - widget

Good to set meta like this: *<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, user-scalable=no, maximum-scale=1.0, minimum-scale=1.0">*

**Creating widget and data binding**
1. Place your widget component by creating a <heat-map></heat-map> html tag.
2. Bind your stringified data using the component's *data* attribute: <heat-map data="yourDataJson"></heat-map>

**Widget files**
1. heat-map.html : Heat-map webComponent. Contains all the logic.
2. heatmap.css : Separated css file.

**example files**
1. index.js : sample code for api call.
2. index.html : sample website which contains heat-map widget.
3. main.css : sample website styles




##Of course you can use the widget with any popular framework##

The widget is a javascript web component.
It should work with any javascript framework.
[Angular](https://www.sitepen.com/blog/2017/09/14/using-web-components-with-angular/), [Vue](https://alligator.io/vuejs/vue-integrate-web-components/)


Comments:
1. I randomized missing data, like type and shouldSell properties. If the real data is updated you need to delete randomizer method from the widget.
2. Widget uses es6 features. To support old browsers like IE11 you should add Babel polyfill to your website.


**Brigitta Forrai**
